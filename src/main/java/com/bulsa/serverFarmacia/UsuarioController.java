package com.bulsa.serverFarmacia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by borja on 19/12/2016.
 */
@RestController
public class UsuarioController {
    @Autowired
    private UsuarioRepository repository;



    @RequestMapping("/usuarios")
    public List<Usuario> getComentarios() {

        List<Usuario> listaU= repository.findAll();
        return listaU;
    }

    @RequestMapping("/add_usuario")
    public void addOpinion(@RequestParam(value = "nombre", defaultValue = "nada") String nombre,
                           @RequestParam(value = "email" , defaultValue = "nada") String email,
                           @RequestParam(value = "pass", defaultValue = "nada") String pass) {

        Usuario u = new Usuario();
        u.setNombre(nombre);
        u.setEmail(email);
        u.setPass(pass);

        repository.save(u);
    }
}

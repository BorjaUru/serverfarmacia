package com.bulsa.serverFarmacia;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by borja on 19/12/2016.
 */
public interface ComentarioRepository extends CrudRepository<Comentario, Integer> {
    List<Comentario> findAll();
}

package com.bulsa.serverFarmacia;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.web.SpringServletContainerInitializer;

@SpringBootApplication
public class ServidorFarmaciaApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ServidorFarmaciaApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	private static Class<ServidorFarmaciaApplication> applicationClass = ServidorFarmaciaApplication.class;
}
